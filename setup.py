from setuptools import setup, find_packages

setup(name='voice_assistant',
      version='0.1',
      description='A voice assistant for personal use.',
      url='https://codeberg.org/rasmusthog/voice-assistant-poc',
      author='Rasmus Vester Thøgersen',
      author_email='codeberg@rasmusthog.me',
      license='AGPL-3.0-or-later',
      packages=find_packages(),
      zip_safe=False)